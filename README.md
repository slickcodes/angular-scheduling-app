## Scheduler Application ##
This is a real time scheduling application where people can book times to meet with you. This concept is inspired from https://scotch.io/tutorials/build-a-real-time-scheduling-app-using-angularjs-and-firebase tutorial.  

**Requirements:**

* Show available dates and availability.
* If someone books a slot make it unavailable for everyone
* Use AngularJs / HTML / CSS (SASS) / Bootstrap as the frontend
* Use Firebase as the backend

**Demo:**
http://ng-schedule.slickcodes.com/

## Commercial Use Cases ##
* Booking cinema seats
* Booking for meetings