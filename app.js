// this tutorial is inpired by https://scotch.io/tutorials/build-a-real-time-scheduling-app-using-angularjs-and-firebase
(function(){
	var app = angular.module('ScheduleApp', ['firebase']);

	app.controller('mainController', function($scope, $firebaseObject){

		// this controller
		var _this = this;

		// create a firebase database connection
		var ref = firebase.database().ref();
		
		// bind angular object to firebase object
		//$scope.data = $firebaseObject(ref.child("days"));


		// double way data binding
		var ref2 = firebase.database().ref().push();
		var profileRef = ref.child("days");
		$scope.data2 = $firebaseObject(profileRef);
		$scope.reset = function(){
			// reset data
			$scope.data.monday = _this.resetData('Monday');
			$scope.data.tuesday = _this.resetData('Tuesday');
			$scope.data.wednesday = _this.resetData('Wednesday');


			$scope.data.$save().then(function() {
				//alert('Profile saved!');
			}).catch(function(error) {
				//alert('Error!');
			});
		}
		

		// three way data binding
		var ref3 = firebase.database().ref().push();
		var roomRef = ref.child('days');
		$firebaseObject(roomRef).$bindTo($scope, "data");


		// this function resets the data
		_this.resetData = function(_day){
			
	        return {
	        	name: _day,
		        slots: {
		          0900: {
		            time: '9:00am',
		            booked: false
		          },
		          1000: {
		            time: '10:00am',
		            booked: false
		          },
		           1100: {
		            time: '11:00am',
		            booked: false
		          },
		           1200: {
		            time: '12:00pm',
		            booked: false
		          },
		           1300: {
		            time: '1:00pm',
		            booked: false
		          }
		        }
	        }
	    }
					   

	});

})();